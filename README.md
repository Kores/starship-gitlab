# 🏭 starship-gitlab

**This is not an official [starship.rs](https://starship.rs) project.**

WIP project for showing GitLab Pipeline status on terminal with [starship.rs](https://starship.rs).

## Installation

Currently, `starship-gitlab` is only installable through `cargo` by running:

```shell
cargo install starship-gitlab
```

## Installation from source:

```shell
cargo install --git https://gitlab.com/Kores/starship-gitlab
```

> *May need additional dependencies, such as `git`*

## Configuration

Update your `~/.config/starship.toml` adding the following lines:

```toml
[custom.starship_gitlab]
command = "starship-gitlab"
directories = [".git"]
format = "[$output]($style)"
```

## Example

![Example](example.png)

## Configure your PAT token for a Gitlab host

To retrieve Pipeline information of private repos you will need to register an access token.

### Personal Token

```shell
starship-gitlab token-set --host gitlab.com --personal-token TOKEN
```

### Project Token

```shell
starship-gitlab token-set --host gitlab.com --project-token TOKEN
```

### OAuth2 Token

```shell
starship-gitlab token-set --host gitlab.com --oauth2-token TOKEN
```

## Caching

starship-gitlab uses [sled](https://crates.io/sled) database for caching, this reduces the amount of queries to [Gitlab](https://gitlab.com) and avoids expressive lag in command execution.

### Rate Limit

Gitlab does have a *rate-limit*, reported in HTTP Headers when requesting resources to Gitlab API. At the time that this documentation was written, there was rate limit of 2000 unauthenticated requests.

These values can be found [here](https://docs.gitlab.com/ee/user/gitlab_com/index.html#gitlabcom-specific-rate-limits), and may change in the future.

However, caching is made to make the experience more fluid (and not because of rate limit), since requests take a bit of time (even in expressively fast connection) and affects the experience negatively.

### Cache location
Cache data is stored in `~/.cache/starship-gitlab`, 

### Cache conditions

In order to be cached, the Pipeline must be in one of those states:

- _success_
- _failed_
- _canceled_
- _skipped_

Other states are not cache, this means that a request is done to check the pipeline status everytime, which may slow down commands in projects with Gitlab remote.

### Cache invalidation

Cache invalidation happens for revision when there is a new commit or there is commits that are not pushed to the remote.

Also, cache can be invalidated with `--clear` argument:

```shell
starship-gitlab --clear
```