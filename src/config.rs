//     starship-gitlab - GitLab Custom Command for starship.rs
//
//         The MIT License (MIT)
//
//      Copyright (c) KoresFramework (https://gitlab.com/Kores/)
//      Copyright (c) contributors
//
//      Permission is hereby granted, free of charge, to any person obtaining a copy
//      of this software and associated documentation files (the "Software"), to deal
//      in the Software without restriction, including without limitation the rights
//      to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//      copies of the Software, and to permit persons to whom the Software is
//      furnished to do so, subject to the following conditions:
//
//      The above copyright notice and this permission notice shall be included in
//      all copies or substantial portions of the Software.
//
//      THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//      IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//      FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//      AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//      THE SOFTWARE.
use std::collections::HashMap;

use crate::token::{Token, TokenType};
use crate::Error;
use serde::Deserialize;
use serde::Serialize;
use std::fs::{File, OpenOptions};
use std::io::{Read, Seek, SeekFrom, Write};

#[derive(Serialize, Deserialize)]
struct Config {
    host: HashMap<String, HostConfig>,
}

#[derive(Serialize, Deserialize)]
struct HostConfig {
    token_type: Option<TokenType>,
    token: Option<String>,
}

pub(crate) fn known_hosts() -> Result<Vec<String>, Error> {
    let config = dirs::config_dir()
        .ok_or(Error::CouldNotFindConfigDirError)?
        .join("starship-gitlab.toml");

    if !config.exists() {
        return Err(Error::CouldNotFindConfigFileError);
    }

    let mut config_content =
        File::open(config).map_err(|e| Error::OpenConfigError(e.to_string()))?;

    let mut content = String::new();

    config_content
        .read_to_string(&mut content)
        .map_err(|e| Error::ReadConfigError(e.to_string()))?;

    let config: Config = toml::from_str(content.as_str())
        .map_err(|e| Error::ReadConfigSyntaxError(e.to_string()))?;

    let hosts = config
        .host
        .keys()
        .map(|s| s.to_string())
        .collect::<Vec<String>>();

    return Ok(hosts);
}

pub(crate) fn token_for_host(host: &String) -> Result<Token, Error> {
    let config = dirs::config_dir()
        .ok_or(Error::CouldNotFindConfigDirError)?
        .join("starship-gitlab.toml");

    if !config.exists() {
        return Err(Error::CouldNotFindConfigFileError);
    }

    let mut config_content =
        File::open(config).map_err(|e| Error::OpenConfigError(e.to_string()))?;

    let mut content = String::new();

    config_content
        .read_to_string(&mut content)
        .map_err(|e| Error::ReadConfigError(e.to_string()))?;

    let config: Config = toml::from_str(content.as_str())
        .map_err(|e| Error::ReadConfigSyntaxError(e.to_string()))?;

    if let Some(hostConfig) = config.host.get(host) {
        if let Some(token) = &hostConfig.token {
            if let Some(token_type) = &hostConfig.token_type {
                return Ok(token_type.create_token(token.to_string()));
            }
        }
    }

    Err(Error::CouldNotFindConfigFileError)
}

pub(crate) fn store_host_config(host: &String, token: Option<Token>) -> Result<(), Error> {
    let config_dir = dirs::config_dir().ok_or(Error::CouldNotFindConfigDirError)?;
    let config = config_dir.join("starship-gitlab.toml");

    if !config_dir.exists() {
        std::fs::create_dir(config_dir.clone())
            .map_err(|e| Error::CreateConfigError(format!("{:?}", e)))?;
    }

    let mut config_file = OpenOptions::new()
        .create(true)
        .write(true)
        .read(true)
        .open(config)
        .map_err(|e| Error::OpenConfigError(e.to_string()))?;

    let mut content = String::new();

    config_file
        .read_to_string(&mut content)
        .map_err(|e| Error::ReadConfigError(e.to_string()))?;

    let mut config: Config = if let Ok(c) = toml::from_str::<Config>(content.as_str()) {
        c
    } else {
        Config {
            host: HashMap::new(),
        }
    };

    config.host.insert(
        host.clone(),
        HostConfig {
            token_type: if let Some(token) = &token {
                Some(token.token_type().clone())
            } else {
                None
            },
            token: if let Some(token) = &token {
                Some(token.token_string().clone())
            } else {
                None
            },
        },
    );

    let new_content =
        toml::to_string(&config).map_err(|e| Error::WriteConfigSyntaxError(e.to_string()))?;

    config_file
        .set_len(0u64)
        .map_err(|e| Error::ClearConfigError(e.to_string()))?;

    config_file
        .seek(SeekFrom::Start(0))
        .map_err(|e| Error::WriteConfigError(e.to_string()))?;

    let as_b = new_content.as_bytes();
    config_file
        .write_all(as_b)
        .map_err(|e| Error::WriteConfigError(e.to_string()))?;

    Ok(())
}
