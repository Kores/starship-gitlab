//     starship-gitlab - GitLab Custom Command for starship.rs
//
//         The MIT License (MIT)
//
//      Copyright (c) KoresFramework (https://gitlab.com/Kores/)
//      Copyright (c) contributors
//
//      Permission is hereby granted, free of charge, to any person obtaining a copy
//      of this software and associated documentation files (the "Software"), to deal
//      in the Software without restriction, including without limitation the rights
//      to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//      copies of the Software, and to permit persons to whom the Software is
//      furnished to do so, subject to the following conditions:
//
//      The above copyright notice and this permission notice shall be included in
//      all copies or substantial portions of the Software.
//
//      THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//      IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//      FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//      AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//      THE SOFTWARE.
use crate::git_api::GitApi;
use crate::gitlab_cache::GitlabCache;
use crate::token::{Token, TokenType};
use crate::Error;
use gitlab::api::AsyncQuery;
use gitlab::AsyncGitlab;
use log::info;
use serde::Deserialize;
use serde::Serialize;

#[derive(Debug, Deserialize, Serialize)]
pub(crate) struct Project {
    pub(crate) id: u64,
    pub(crate) name: String,
}

#[derive(Debug, Deserialize, Serialize, Clone)]
pub(crate) struct Pipeline {
    pub(crate) id: u64,
    pub(crate) status: String,
    #[serde(alias = "ref")]
    pub(crate) ref_: String,
}

pub(crate) struct GitlabClient {
    host: String,
    client: AsyncGitlab,
    cache: GitlabCache,
    token: Option<Token>,
}

impl GitlabClient {
    pub(crate) async fn new(host: String, token: Option<Token>) -> Result<GitlabClient, Error> {
        let client = if let Some(t) = &token {
            let mut builder = gitlab::GitlabBuilder::new(host.clone(), t);

            if let TokenType::OAuth2 = &t.token_type() {
                builder
                    .oauth2_token()
                    .build_async()
                    .await
                    .map_err(|e| Error::FailedToBuildGitlabClient(format!("{:?}", e)))?
            } else {
                builder
                    .build_async()
                    .await
                    .map_err(|e| Error::FailedToBuildGitlabClient(format!("{:?}", e)))?
            }
        } else {
            gitlab::GitlabBuilder::new_unauthenticated(host.clone())
                .build_async()
                .await
                .map_err(|e| Error::FailedToBuildGitlabClient(format!("{:?}", e)))?
        };
        Ok(GitlabClient {
            host,
            client,
            cache: GitlabCache::new()?,
            token,
        })
    }

    pub(crate) async fn read_project(&self, name: String) -> Result<Project, Error> {
        let endpoint = gitlab::api::projects::Project::builder()
            .project(name)
            .build()
            .map_err(|e| Error::FailedToBuildEndpoint(format!("{:?}", e)))?;
        let project: Project = endpoint
            .query_async(&self.client)
            .await
            .map_err(|e| Error::FailedToQueryProject(format!("{:?}", e)))?;

        return Ok(project);
        //https://gitlab.com/api/v4/projects/
    }

    pub(crate) async fn read_project_from_cache_or_network(
        &mut self,
        name: String,
    ) -> Result<Project, Error> {
        if let Ok(proj) = self.cache.read_project(&self.host, &name) {
            Ok(proj)
        } else {
            let proj = self.read_project(name.clone()).await?;
            self.cache.store_project(&self.host, &name, &proj)?;
            Ok(proj)
        }
    }

    pub(crate) async fn read_pipeline(
        &self,
        id: u64,
        branch: &str,
    ) -> Result<Vec<Pipeline>, Error> {
        let endpoint = gitlab::api::projects::pipelines::Pipelines::builder()
            .project(id)
            .ref_(branch)
            .build()
            .map_err(|e| Error::FailedToBuildEndpoint(format!("{:?}", e)))?;

        let pipelines: Vec<Pipeline> = endpoint
            .query_async(&self.client)
            .await
            .map_err(|e| Error::FailedToQueryPipelines(format!("{:?}", e)))?;

        return Ok(pipelines);
    }

    pub(crate) async fn read_pipeline_from_cache_or_network(
        &mut self,
        api: &GitApi,
        project: &Project,
        time: i64,
        branch: &String,
    ) -> Result<Vec<Pipeline>, Error> {
        let should_lookup = !api.has_un_pushed_commits(branch);
        let cached = if should_lookup {
            self.cache
                .read_pipelines(&self.host, &project.name, time, &branch)
        } else {
            info!(
                "Invalidating caches of project {} for revision {}. Reason: Un-pushed commits.",
                project.name, branch
            );
            Err(Error::ProjectNotFoundError)
        };

        if let Ok(pipelines) = cached {
            Ok(pipelines)
        } else {
            let proj: Vec<Pipeline> = self.read_pipeline(project.id, &branch).await?;

            let cache_pipelines: Vec<Pipeline> = proj
                .to_vec()
                .into_iter()
                .filter(|p| GitlabClient::should_cache(p))
                .collect();

            let clear_pipelines: Vec<Pipeline> = proj
                .to_vec()
                .into_iter()
                .filter(|p| !GitlabClient::should_cache(p))
                .collect();

            if !clear_pipelines.is_empty() {
                self.cache
                    .clear_pipelines(&self.host, &project.name, &branch)?;
            } else if !cache_pipelines.is_empty() {
                self.cache.store_pipelines(
                    &self.host,
                    &project.name,
                    time,
                    &branch,
                    &cache_pipelines,
                )?;
            } else if proj.is_empty() {
                self.cache
                    .store_pipelines(&self.host, &project.name, time, &branch, &proj)?;
            }

            Ok(proj)
        }
    }

    fn should_cache(p: &Pipeline) -> bool {
        p.status == "success"
            || p.status == "failed"
            || p.status == "canceled"
            || p.status == "skipped"
    }
}
