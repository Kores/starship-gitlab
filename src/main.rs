//     starship-gitlab - GitLab Custom Command for starship.rs
//
//         The MIT License (MIT)
//
//      Copyright (c) KoresFramework (https://gitlab.com/Kores/)
//      Copyright (c) contributors
//
//      Permission is hereby granted, free of charge, to any person obtaining a copy
//      of this software and associated documentation files (the "Software"), to deal
//      in the Software without restriction, including without limitation the rights
//      to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//      copies of the Software, and to permit persons to whom the Software is
//      furnished to do so, subject to the following conditions:
//
//      The above copyright notice and this permission notice shall be included in
//      all copies or substantial portions of the Software.
//
//      THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//      IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//      FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//      AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//      THE SOFTWARE.
mod config;
mod git_api;
mod gitlab_api;
mod gitlab_cache;
mod token;

use crate::config::{known_hosts, store_host_config, token_for_host};
use crate::git_api::GitApi;
use crate::gitlab_api::{GitlabClient, Pipeline};
use crate::gitlab_cache::GitlabCache;
use crate::token::Token;
use clap::{AppSettings, Parser};
use git2::Repository;
use git_url_parse::GitUrl;
use itertools::Itertools;
use simplelog::{Config, LevelFilter};
use termion::color;

#[derive(Parser)]
#[clap(version = "1.0", author = "Jonathan H. R. Lopes <jhrldev@gmail.com>")]
struct Opts {
    #[clap(
        short,
        long,
        about = "The Gitlab host, used to detect if project exists on Gitlab and to query Pipeline information"
    )]
    gitlab_host: Option<String>,
    #[clap(
        short,
        long,
        default_value = "master",
        about = "This branch will be queried alongside the current branch everytime"
    )]
    main: String,
    #[clap(long, about = "Clears the starship-gitlab global cache")]
    clear: bool,
    /*#[clap(short, long, default_value = "3")]
    commits: usize,*/
    #[clap(
        short,
        long,
        default_value = "3",
        about = "Amount of tags to query alongside current branch and main branch"
    )]
    tags: usize,
    #[clap(
        short,
        long,
        about = "Query Pipelines of latest created tags alongside current and main branches"
    )]
    last_tags: bool,
    #[clap(
        short,
        long,
        about = "Verbose mode, configures the logger to INFO Level and prints cache invalidations"
    )]
    verbose: bool,
    #[clap(flatten, about = "Pipeline status format configuration")]
    format: Format,
    #[clap(subcommand)]
    host_config: Option<HostConfigCommand>,
}

#[derive(Parser)]
struct Format {
    #[clap(long, default_value = "🏭")]
    prefix: String,
    #[clap(long, default_value = "📖")]
    created: String,
    #[clap(long, default_value = "💻")]
    waiting_for_resource: String,
    #[clap(long, default_value = "🔨")]
    preparing: String,
    #[clap(long, default_value = "◌")]
    pending: String,
    #[clap(long, default_value = "➜")]
    running: String,
    #[clap(long, default_value = "✔")]
    success: String,
    #[clap(long, default_value = "⨯")]
    failure: String,
    #[clap(long, default_value = "◼")]
    canceled: String,
    #[clap(long, default_value = "»")]
    skipped: String,
    #[clap(long, default_value = "🖐")]
    manual: String,
    #[clap(long, default_value = "⏳")]
    scheduled: String,
}

#[derive(Parser)]
enum HostConfigCommand {
    #[clap(name = "token-set")]
    TokenSet(TokenSet),
    #[clap(name = "add-host")]
    AddHost(AddHost),
}

#[derive(Parser)]
#[clap(
    name = "token-set",
    about = "Sets a global Token for the provided host"
)]
struct TokenSet {
    #[clap(
        long,
        about = "Host to configure token to, omit to use host of current repo"
    )]
    host: Option<String>,
    #[clap(
        long,
        about = "Sets a global Personal Access Token for the provided host"
    )]
    personal_token: Option<String>,
    #[clap(
        long,
        about = "Sets a global Project Access Token for the provided host"
    )]
    project_token: Option<String>,
    #[clap(long, about = "Sets a global OAuth2 for the provided host")]
    oauth2_token: Option<String>,
}

#[derive(Parser)]
#[clap(
    name = "add-host",
    about = "Adds the host to known hosts in configuration."
)]
struct AddHost {
    #[clap(long, about = "Host to add to known hosts")]
    host: String,
}

#[derive(Debug, Clone)]
enum Error {
    RepoLoadError(String),
    RemoteReadError(String),
    UrlParseError(String),
    ReadTagError,
    FindRefError(String),
    FindRefOidError,
    NoCommitsError,
    CouldNotClearKeyError(String, String),
    CouldNotClearDbError(String),
    CouldNotSetVersionError(String),
    CouldNotOpenDbError(String),
    CouldNotReadVersionError(String),
    CouldNotCacheProjectError(String),
    CouldReadCacheProjectError(String),
    ProjectNotFoundError,
    VersionNotFoundError,
    FindCommitError(String),
    RetrieveTagError(String),
    MissingHeadError(String),
    ShortHandNameError,
    NotGitLabUrl(String),
    NoKnownGitLabUrlFound,
    NoRemoteUrls,
    MultiErrors(Vec<Error>),
    FailedToBuildEndpoint(String),
    FailedToQueryProject(String),
    FailedToQueryPipelines(String),
    FailedToBuildGitlabClient(String),
    ExtractHostError(String),
    CouldNotFindCacheDirError,
    CouldNotFindConfigDirError,
    CouldNotFindConfigFileError,
    // Handled
    CreateConfigError(String),
    OpenConfigError(String),
    ReadConfigError(String),
    ClearConfigError(String),
    WriteConfigError(String),
    WriteConfigSyntaxError(String),
    ReadConfigSyntaxError(String),
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    let opts: Opts = Opts::parse();

    if opts.verbose {
        simplelog::SimpleLogger::init(LevelFilter::Info, Config::default()).unwrap();
    }

    if opts.clear {
        return GitlabCache::new().and_then(|mut f| f.clear());
    }

    let hosts = if let Some(host) = &opts.gitlab_host {
        vec![host.to_string()]
    } else {
        known_hosts().unwrap_or(vec!["gitlab.com".to_string()])
    };

    if let Some(HostConfigCommand::AddHost(add_host)) = &opts.host_config {
        return store_host_config(&add_host.host, None);
    }

    let repo_result = load_repo();
    let git = repo_result.map(|repo| GitApi::new(repo));
    let gitlab_url = git.as_ref().and_then(|git| {
        for host in hosts {
            if let Ok(git_url) = git.find_gitlab(&host) {
                return Ok(git_url);
            }
        }
        return Err(&Error::NoKnownGitLabUrlFound);
    });

    if let Some(HostConfigCommand::TokenSet(set)) = &opts.host_config {
        let host = if let Some(host) = &set.host {
            Ok(host.to_string())
        } else {
            if let Ok(ref gitlab_url) = gitlab_url {
                if let Some(host) = &gitlab_url.host {
                    Ok(host.clone())
                } else {
                    Err(Error::NoKnownGitLabUrlFound)
                }
            } else if let Err(e) = gitlab_url {
                Err(e.clone())
            } else {
                Err(Error::NoKnownGitLabUrlFound)
            }
        }?;

        if let Some(personal) = &set.personal_token {
            return store_host_config(&host, Some(Token::PersonalToken(personal.to_string())));
        } else if let Some(project) = &set.project_token {
            return store_host_config(&host, Some(Token::ProjectToken(project.to_string())));
        } else if let Some(oauth2) = &set.oauth2_token {
            return store_host_config(&host, Some(Token::OAuth2(oauth2.to_string())));
        }
    }
    //if opts.set_pat.set_oauth2_token.is_some() || opts.set_pat.set_project_token.is_some() {}

    let git = if let Ok(git) = &git {
        git
    } else {
        return Err(git.err().unwrap().clone());
    };

    let head = git.current_head()?;
    let head_name = head.name();
    let gitlab = if let Ok(ref url) = &gitlab_url {
        url.clone()
    } else {
        return Err(gitlab_url.err().unwrap().clone());
    };

    let host = gitlab
        .clone()
        .host
        .ok_or(Error::ExtractHostError(format!("{:?}", gitlab)))?;

    let token_result = token_for_host(&host);

    if let Err(err) = &token_result {
        if let Error::ReadConfigSyntaxError(e) = err {
            return Err(err.clone());
        } else if let Error::OpenConfigError(e) = err {
            return Err(err.clone());
        } else if let Error::ReadConfigError(e) = err {
            return Err(err.clone());
        } else if let Error::ClearConfigError(e) = err {
            return Err(err.clone());
        }
    }

    let token = token_result.ok();

    let full_name = path(&gitlab);
    let time = git.last_commit_time()?;
    let mut client = GitlabClient::new(host, token).await?;
    let project = client.read_project_from_cache_or_network(full_name).await?;
    let pipelines = client
        .read_pipeline_from_cache_or_network(&git, &project, time, &head_name)
        .await?;

    let mut messages: Vec<String> = vec![];

    messages.extend(get_single_pipeline_status_message(
        &opts, &head_name, pipelines,
    ));

    if head_name != opts.main {
        let main_pipelines = client
            .read_pipeline_from_cache_or_network(&git, &project, time, &opts.main)
            .await?;

        messages.extend(get_single_pipeline_status_message(
            &opts,
            &opts.main,
            main_pipelines,
        ));
    }

    if opts.last_tags {
        if let Ok(tag) = git.last_tags(opts.tags) {
            /*let mut commit_pipelines: Vec<Pipeline> = vec![];
            for c in commits {
                let pipelines = client
                    .read_pipeline(project.id, c.commit.id().to_string().as_str())
                    .await?;

                for pipe in pipelines {
                    commit_pipelines.push(pipe);
                }
            }*/

            for t in tag {
                let pipelines = client
                    .read_pipeline_from_cache_or_network(&git, &project, time, &t.tag)
                    .await?;
                let pipe: Vec<Pipeline> = pipelines.into_iter().take(1).collect();

                messages.push(get_pipeline_status_message(&opts, &t.tag, &pipe));
            }
        }
    }

    if !messages.is_empty() {
        print!("{} {}", opts.format.prefix, messages.join(" "))
    }

    Ok(())
}

fn path(url: &GitUrl) -> String {
    let path = &url.path;
    if path.ends_with(".git") {
        let end = path.len() - ".git".len();
        (&path[..end]).to_string()
    } else {
        path.to_string()
    }
}

fn get_single_pipeline_status_message(
    opts: &Opts,
    ref_name: &String,
    pipelines: Vec<Pipeline>,
) -> Vec<String> {
    let mut messages: Vec<String> = vec![];
    for p in pipelines {
        let pipe = vec![p];
        let msg = get_pipeline_status_message(&opts, ref_name, &pipe);
        messages.push(msg);
        break;
    }
    messages
}

fn get_pipeline_status_message(
    opts: &Opts,
    ref_name: &String,
    pipelines: &Vec<Pipeline>,
) -> String {
    pipelines
        .iter()
        .map(|p| match p.status.as_ref() {
            "created" => Some(format!(
                "{}[{} {}]",
                color::Fg(color::LightCyan),
                ref_name,
                &opts.format.created
            )),
            "waiting_for_resource" => Some(format!(
                "{}[{} {}]",
                color::Fg(color::Yellow),
                ref_name,
                &opts.format.waiting_for_resource
            )),
            "preparing" => Some(format!(
                "{}[{} {}]",
                color::Fg(color::Blue),
                ref_name,
                &opts.format.preparing
            )),
            "pending" => Some(format!(
                "{}[{} {}]",
                color::Fg(color::LightBlue),
                ref_name,
                &opts.format.pending
            )),
            "running" => Some(format!(
                "{}[{} {}]",
                color::Fg(color::LightYellow),
                ref_name,
                &opts.format.running
            )),
            "success" => Some(format!(
                "{}[{} {}]",
                color::Fg(color::Cyan),
                ref_name,
                &opts.format.success
            )),
            "failed" => Some(format!(
                "{}[{} {}]",
                color::Fg(color::Red),
                ref_name,
                &opts.format.failure
            )),
            "canceled" => Some(format!(
                "{}[{} {}]",
                color::Fg(color::Cyan),
                ref_name,
                &opts.format.failure
            )),
            "skipped" => Some(format!(
                "{}[{} {}]",
                color::Fg(color::Reset),
                ref_name,
                &opts.format.failure
            )),
            "manual" => Some(format!(
                "{}[{} {}]",
                color::Fg(color::LightBlue),
                ref_name,
                &opts.format.failure
            )),
            "scheduled" => Some(format!(
                "{}[{} {}]",
                color::Fg(color::LightYellow),
                ref_name,
                &opts.format.failure
            )),
            _ => None,
        })
        .filter(|p| p.is_some())
        .map(|p| p.unwrap())
        .join(" ")
}

fn load_repo() -> Result<Repository, Error> {
    match Repository::open(".") {
        Ok(repo) => Ok(repo),
        Err(e) => Err(Error::RepoLoadError(e.to_string())),
    }
}
