//     starship-gitlab - GitLab Custom Command for starship.rs
//
//         The MIT License (MIT)
//
//      Copyright (c) KoresFramework (https://gitlab.com/Kores/)
//      Copyright (c) contributors
//
//      Permission is hereby granted, free of charge, to any person obtaining a copy
//      of this software and associated documentation files (the "Software"), to deal
//      in the Software without restriction, including without limitation the rights
//      to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//      copies of the Software, and to permit persons to whom the Software is
//      furnished to do so, subject to the following conditions:
//
//      The above copyright notice and this permission notice shall be included in
//      all copies or substantial portions of the Software.
//
//      THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//      IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//      FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//      AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//      THE SOFTWARE.
use serde::Deserialize;
use serde::Serialize;

#[derive(Serialize, Deserialize, Clone)]
pub(crate) enum TokenType {
    OAuth2,
    Personal,
    Project,
    CI,
}

#[derive(Serialize, Deserialize, Clone)]
pub(crate) enum Token {
    OAuth2(String),
    PersonalToken(String),
    ProjectToken(String),
    CiToken(String),
}

impl TokenType {
    pub(crate) fn create_token(&self, token: String) -> Token {
        match self {
            TokenType::OAuth2 => Token::OAuth2(token),
            TokenType::Personal => Token::PersonalToken(token),
            TokenType::Project => Token::ProjectToken(token),
            TokenType::CI => Token::CiToken(token),
        }
    }
}

impl Token {
    pub(crate) fn token_type(&self) -> TokenType {
        match self {
            Token::OAuth2(_) => TokenType::OAuth2,
            Token::PersonalToken(_) => TokenType::Personal,
            Token::ProjectToken(_) => TokenType::Project,
            Token::CiToken(_) => TokenType::CI,
        }
    }

    pub(crate) fn token_string(&self) -> String {
        match self {
            Token::OAuth2(s) => s,
            Token::PersonalToken(s) => s,
            Token::ProjectToken(s) => s,
            Token::CiToken(s) => s,
        }
        .to_string()
    }
}

impl Into<String> for Token {
    fn into(self) -> String {
        self.token_string()
    }
}

impl Into<String> for &Token {
    fn into(self) -> String {
        self.token_string()
    }
}
