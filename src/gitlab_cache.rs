//     starship-gitlab - GitLab Custom Command for starship.rs
//
//         The MIT License (MIT)
//
//      Copyright (c) KoresFramework (https://gitlab.com/Kores/)
//      Copyright (c) contributors
//
//      Permission is hereby granted, free of charge, to any person obtaining a copy
//      of this software and associated documentation files (the "Software"), to deal
//      in the Software without restriction, including without limitation the rights
//      to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//      copies of the Software, and to permit persons to whom the Software is
//      furnished to do so, subject to the following conditions:
//
//      The above copyright notice and this permission notice shall be included in
//      all copies or substantial portions of the Software.
//
//      THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//      IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//      FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//      AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//      THE SOFTWARE.

use crate::gitlab_api::{Pipeline, Project};
use crate::Error;
use log::info;
use serde::Deserialize;
use serde::Serialize;
use sled::{Db, IVec};
use std::fs;
use std::path::PathBuf;
use std::str::FromStr;

const CACHE_VERSION: &[u8; 5] = b"0.0.3";

#[derive(Serialize, Deserialize, Debug, Clone)]
pub(crate) struct ProjectKey {
    pub(crate) path: KeyPath,
    pub(crate) host: String,
    pub(crate) name: String,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub(crate) enum KeyPath {
    Data,
    Time,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub(crate) struct ProjectPipelineKey {
    pub(crate) path: KeyPath,
    pub(crate) host: String,
    pub(crate) name: String,
    pub(crate) revision: String,
}

pub(crate) struct GitlabCache {
    cache_root: PathBuf,
    cache_db_file: PathBuf,
    db: Db,
}

impl Into<IVec> for Project {
    fn into(self) -> IVec {
        IVec::from(serde_json::to_string(&self).unwrap().as_str())
    }
}

impl Into<IVec> for &Project {
    fn into(self) -> IVec {
        IVec::from(serde_json::to_string(&self).unwrap().as_str())
    }
}

impl Into<IVec> for &Pipeline {
    fn into(self) -> IVec {
        IVec::from(serde_json::to_string(&self).unwrap().as_str())
    }
}

impl From<IVec> for Project {
    fn from(vec: IVec) -> Self {
        serde_json::from_slice(&vec).unwrap()
    }
}

impl From<IVec> for Pipeline {
    fn from(vec: IVec) -> Self {
        serde_json::from_slice(&vec).unwrap()
    }
}

impl Into<IVec> for ProjectKey {
    fn into(self) -> IVec {
        IVec::from(serde_json::to_string(&self).unwrap().as_str())
    }
}

impl Into<IVec> for ProjectPipelineKey {
    fn into(self) -> IVec {
        IVec::from(serde_json::to_string(&self).unwrap().as_str())
    }
}

impl From<IVec> for ProjectKey {
    fn from(vec: IVec) -> Self {
        serde_json::from_slice(&vec).unwrap()
    }
}

impl From<IVec> for ProjectPipelineKey {
    fn from(vec: IVec) -> Self {
        serde_json::from_slice(&vec).unwrap()
    }
}

impl GitlabCache {
    pub(crate) fn new() -> Result<GitlabCache, Error> {
        let cache = dirs::cache_dir()
            .ok_or(Error::CouldNotFindCacheDirError)?
            .join("starship-gitlab");

        let db = cache.join("db");

        if !cache.exists() {
            fs::create_dir(cache.clone())
                .map_err(|e| Error::CouldNotOpenDbError(format!("{:?}", e)))?;
        }

        let tree = sled::open(&db).map_err(|e| Error::CouldNotOpenDbError(format!("{:?}", e)))?;

        {
            if let Ok(Some(v)) = tree.get(b"version") {
                if v != CACHE_VERSION {
                    // TODO: Abstract cache miss reason
                    info!("Invalidating caches of all projects. Reason: Mismatch cache version.");
                    tree.clear()
                        .map_err(|e| Error::CouldNotClearDbError(format!("{:?}", e)))?;
                }
            }
        }

        {
            tree.insert("version", CACHE_VERSION)
                .map_err(|e| Error::CouldNotSetVersionError(format!("{:?}", e)))?;
        }

        Ok(GitlabCache {
            cache_root: cache,
            cache_db_file: db,
            db: tree,
        })
    }

    pub(crate) fn read_version(&self) -> Result<String, Error> {
        let data = &self
            .db
            .get(b"version")
            .map_err(|e| Error::CouldNotReadVersionError(format!("{:?}", e)))?
            .ok_or(Error::VersionNotFoundError)?;

        Ok(String::from_utf8_lossy(data).to_string())
    }

    pub(crate) fn store_project(
        &mut self,
        host: &String,
        name: &String,
        project: &Project,
    ) -> Result<(), Error> {
        let key = ProjectKey {
            path: KeyPath::Data,
            host: host.clone(),
            name: name.clone(),
        };
        let key_bytes: IVec = key.into();
        let project_bytes: IVec = project.into();

        self.db
            .insert(key_bytes, project_bytes)
            .map_err(|e| Error::CouldNotCacheProjectError(format!("{:?}", e)))?;

        Ok(())
    }

    pub(crate) fn store_pipelines(
        &mut self,
        host: &String,
        name: &String,
        time: i64,
        revision: &String,
        pipelines: &Vec<Pipeline>,
    ) -> Result<(), Error> {
        let data_key = ProjectPipelineKey {
            path: KeyPath::Data,
            host: host.clone(),
            name: name.clone(),
            revision: revision.clone(),
        };

        let time_key = ProjectPipelineKey {
            path: KeyPath::Time,
            host: host.clone(),
            name: name.clone(),
            revision: revision.clone(),
        };

        let data_key_bytes: IVec = data_key.into();
        let time_key_bytes: IVec = time_key.into();
        let project_bytes: IVec = IVec::from(serde_json::to_string(&pipelines).unwrap().as_str());

        self.db
            .insert(data_key_bytes, project_bytes)
            .map_err(|e| Error::CouldNotCacheProjectError(format!("{:?}", e)))?;

        self.db
            .insert(time_key_bytes, i64::to_string(&time).as_bytes())
            .map_err(|e| Error::CouldNotCacheProjectError(format!("{:?}", e)))?;

        Ok(())
    }

    pub(crate) fn clear_pipelines(
        &mut self,
        host: &String,
        name: &String,
        revision: &String,
    ) -> Result<(), Error> {
        // TODO: Abstract cache miss reason
        info!(
            "Invalidating caches of project {} for revision {}. Reason: non-cacheable pipeline status.",
            name, revision
        );
        let data_key = ProjectPipelineKey {
            path: KeyPath::Data,
            host: host.clone(),
            name: name.clone(),
            revision: revision.clone(),
        };

        let time_key = ProjectPipelineKey {
            path: KeyPath::Time,
            host: host.clone(),
            name: name.clone(),
            revision: revision.clone(),
        };

        let data_key_bytes: IVec = data_key.clone().into();
        let time_key_bytes: IVec = time_key.clone().into();

        self.db.remove(data_key_bytes).map_err(|e| {
            Error::CouldNotClearKeyError(format!("{:?}", data_key), format!("{:?}", e))
        });
        self.db.remove(time_key_bytes).map_err(|e| {
            Error::CouldNotClearKeyError(format!("{:?}", time_key), format!("{:?}", e))
        });

        Ok(())
    }

    pub(crate) fn read_project(&self, host: &String, name: &String) -> Result<Project, Error> {
        let key = ProjectKey {
            path: KeyPath::Data,
            host: host.clone(),
            name: name.clone(),
        };
        let key_bytes: IVec = key.into();

        let data = self
            .db
            .get(key_bytes)
            .map_err(|e| Error::CouldReadCacheProjectError(format!("{:?}", e)))?
            .ok_or_else(|| Error::ProjectNotFoundError)?;

        Ok(Project::from(data))
    }

    pub(crate) fn read_pipelines(
        &self,
        host: &String,
        name: &String,
        time: i64,
        revision: &String,
    ) -> Result<Vec<Pipeline>, Error> {
        let time_key = ProjectPipelineKey {
            path: KeyPath::Time,
            host: host.clone(),
            name: name.clone(),
            revision: revision.clone(),
        };

        let time_key_bytes: IVec = time_key.into();

        let time_data = self
            .db
            .get(time_key_bytes)
            .map_err(|e| Error::CouldReadCacheProjectError(format!("{:?}", e)))?
            .ok_or_else(|| Error::ProjectNotFoundError)?;

        let time_data_vec = time_data.to_vec();
        let stored_time = i64::from_str(String::from_utf8_lossy(&time_data_vec).as_ref()).unwrap();

        let key = ProjectPipelineKey {
            path: KeyPath::Data,
            host: host.clone(),
            name: name.clone(),
            revision: revision.clone(),
        };

        let key_bytes: IVec = key.into();

        if time > stored_time {
            self.db.remove(key_bytes.clone()).unwrap();
            // TODO: Abstract cache miss reason
            info!(
                "Invalidating caches of project {} for revision {}. Reason: Cache outdated.",
                name, revision
            );
            return Err(Error::ProjectNotFoundError);
        }

        let data = self
            .db
            .get(key_bytes)
            .map_err(|e| Error::CouldReadCacheProjectError(format!("{:?}", e)))?
            .ok_or_else(|| Error::ProjectNotFoundError)?;

        Ok(serde_json::from_slice(&data).unwrap())
    }

    pub(crate) fn clear(&mut self) -> Result<(), Error> {
        // TODO: Abstract cache miss reason
        info!("Invalidating caches of all projects. Reason: User request.");
        self.db
            .clear()
            .map_err(|e| Error::CouldNotClearDbError(format!("{:?}", e)))?;
        self.db
            .flush()
            .map_err(|e| Error::CouldNotClearDbError(format!("{:?}", e)))?;
        Ok(())
    }
}
