//     starship-gitlab - GitLab Custom Command for starship.rs
//
//         The MIT License (MIT)
//
//      Copyright (c) KoresFramework (https://gitlab.com/Kores/)
//      Copyright (c) contributors
//
//      Permission is hereby granted, free of charge, to any person obtaining a copy
//      of this software and associated documentation files (the "Software"), to deal
//      in the Software without restriction, including without limitation the rights
//      to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//      copies of the Software, and to permit persons to whom the Software is
//      furnished to do so, subject to the following conditions:
//
//      The above copyright notice and this permission notice shall be included in
//      all copies or substantial portions of the Software.
//
//      THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//      IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//      FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//      AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//      THE SOFTWARE.

use crate::Error;
use git2::{Commit, Reference, Remote, Repository};
use git_url_parse::GitUrl;
use itertools::Itertools;
use std::fmt::{Debug, Display, Formatter};

#[derive(Debug)]
pub(crate) enum GitHead {
    Branch(String),
    Ref(String),
}

pub(crate) struct GitTag<'r> {
    pub(crate) tag: String,
    pub(crate) ref_: Reference<'r>,
    pub(crate) commit: Commit<'r>,
}

pub(crate) struct GitCommit<'r> {
    pub(crate) commit: Commit<'r>,
}

impl<'r> Display for GitTag<'r> {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.commit.id())
    }
}

impl<'r> Debug for GitTag<'r> {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "GitTag({:?}, {:?})", self.tag, self.commit.id())
    }
}

impl<'r> Display for GitCommit<'r> {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.commit.id())
    }
}

impl<'r> Debug for GitCommit<'r> {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "GitCommit({:?})", self.commit.id())
    }
}

impl GitHead {
    pub(crate) fn name(&self) -> String {
        match self {
            GitHead::Branch(n) => n,
            GitHead::Ref(n) => n,
        }
        .clone()
    }
}

pub(crate) struct GitApi {
    repository: Repository,
}

impl GitApi {
    pub(crate) fn new(repository: Repository) -> GitApi {
        GitApi { repository }
    }

    pub(crate) fn find_gitlab(&self, host: &String) -> Result<GitUrl, Error> {
        let remote = self
            .repository
            .remotes()
            .map_err(|e| Error::RemoteReadError(e.to_string()))?;

        let remotes = remote
            .iter()
            .filter(|f| f.is_some())
            .map(|f| f.unwrap())
            .map(|r| self.repository.find_remote(r))
            .filter(|r| r.is_ok())
            .map(|r| r.unwrap())
            .collect::<Vec<Remote>>();

        let urls = remotes
            .iter()
            .map(|r| r.url())
            .filter(|r| r.is_some())
            .map(|r| r.unwrap())
            .map(|p| GitApi::parse_url(p))
            .map(|p| p.and_then(|v| GitApi::match_url(host, v)))
            .collect::<Vec<Result<GitUrl, Error>>>();

        let success = urls.iter().filter(|p| p.is_ok()).next();

        if let Some(v) = success {
            if let Ok(r) = v {
                return Ok(r.clone());
            }
        }

        let errors = urls
            .into_iter()
            .map(|p| p.unwrap_err())
            .collect::<Vec<Error>>();

        return if errors.is_empty() {
            Err(Error::NoRemoteUrls)
        } else {
            Err(Error::MultiErrors(errors))
        };
    }

    fn match_url(host: &String, git_url: GitUrl) -> Result<GitUrl, Error> {
        if git_url.host.as_ref().map_or(false, |v| v.eq(host)) {
            Ok(git_url)
        } else {
            return Err(Error::NotGitLabUrl(git_url.to_string()));
        }
    }

    fn parse_url(url: &str) -> Result<GitUrl, Error> {
        GitUrl::parse(url).map_err(|f| Error::UrlParseError(f.to_string()))
    }

    pub(crate) fn current_head(&self) -> Result<GitHead, Error> {
        let head_ref = self
            .repository
            .head()
            .map_err(|e| Error::MissingHeadError(e.to_string()))?;

        let short = head_ref.shorthand().ok_or(Error::ShortHandNameError)?;

        if head_ref.is_branch() {
            Ok(GitHead::Branch(short.to_string()))
        } else {
            Ok(GitHead::Ref(short.to_string()))
        }
    }

    pub(crate) fn last_tags(&self, amount: usize) -> Result<Vec<GitTag>, Error> {
        let tags = self
            .repository
            .tag_names(None)
            .map_err(|e| Error::RetrieveTagError(e.to_string()))?;

        let refs = tags
            .iter()
            .filter(|t| t.is_some())
            .map(|t| t.unwrap())
            .map(|t| {
                self.repository
                    .resolve_reference_from_short_name(t)
                    .map_err(|e| Error::FindRefError(e.to_string()))
                    .and_then(|reff| {
                        let target = reff.target().ok_or(Error::FindRefOidError)?;
                        let commit = self
                            .repository
                            .find_commit(target)
                            .map_err(|e| Error::FindCommitError(e.to_string()))?;

                        Ok(GitTag {
                            tag: t.to_string(),
                            ref_: reff,
                            commit,
                        })
                    })
            })
            .collect::<Vec<Result<GitTag, Error>>>();

        let mut unsorted_references = refs
            .into_iter()
            .filter(|f| f.is_ok())
            .map(|f| f.unwrap())
            .collect::<Vec<GitTag>>();

        unsorted_references.sort_by(|a, b| a.commit.time().cmp(&b.commit.time()));

        let references = unsorted_references
            .into_iter()
            .rev()
            .take(amount)
            .collect::<Vec<GitTag>>();

        return if references.is_empty() {
            Err(Error::ReadTagError)
        } else {
            Ok(references)
        };
    }

    pub(crate) fn last_commits(&self, amount: usize) -> Result<Vec<GitCommit>, Error> {
        let rev = self.repository.references().unwrap();
        let commits = rev
            .filter(|v| v.is_ok())
            .map(|v| v.unwrap())
            .filter(|v| v.target().is_some())
            .map(|v| self.repository.find_commit(v.target().unwrap()))
            .filter(|c| c.is_ok())
            .map(|c| c.unwrap())
            .map(|c| GitCommit { commit: c })
            .sorted_by(|a, b| a.commit.time().cmp(&b.commit.time()))
            .unique_by(|c| c.commit.id())
            .rev()
            .take(amount)
            .collect::<Vec<GitCommit>>();

        return if commits.is_empty() {
            Err(Error::NoCommitsError)
        } else {
            Ok(commits)
        };
    }

    pub(crate) fn has_un_pushed_commits(&self, branch: &String) -> bool {
        let branches = self.repository.branches(None).unwrap();
        {
            let filtered_branches = branches
                .filter(|b| b.is_ok())
                .map(|b| b.unwrap())
                .filter(|b| b.0.name().is_ok())
                .filter(|b| b.0.name().unwrap().is_some())
                .filter(|b| b.0.name().unwrap().unwrap() == branch.as_str());

            for b in filtered_branches {
                let branch = b.0;
                let local = branch.get();
                let upstream = branch.upstream();
                if upstream.is_ok() {
                    let up_branch = upstream.unwrap();
                    let remote = up_branch.get();

                    if let Some(local_target) = local.target() {
                        if let Some(remote_target) = remote.target() {
                            if local_target != remote_target {
                                return true;
                            }
                        }
                    }
                }
            }
        }

        false
    }

    pub(crate) fn last_commit_time(&self) -> Result<i64, Error> {
        self.last_commits(1)?
            .get(0)
            .map(|c| c.commit.time())
            .map(|t| t.seconds())
            .ok_or_else(|| Error::NoCommitsError)
    }
}
